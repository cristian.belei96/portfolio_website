from django.forms import ModelForm, TextInput, EmailInput, Textarea
from .models import Contact


class ContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['fullname', 'email', 'subject', 'message']
        widgets = {
            'fullname': TextInput(attrs={'placeholder': 'Enter your Full Name', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Email-ul', 'class': 'form-control'}),
            'subject': TextInput(attrs={'placeholder': 'Enter the subject', 'class': 'form-control'}),
            'message': Textarea(attrs={'placeholder': 'Please enter your massage', 'class': 'form-control'})
        }