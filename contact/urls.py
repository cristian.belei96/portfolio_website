from django.urls import path, include

from contact import views

urlpatterns = [
    path('contact-view/', views.ContactTemplateView.as_view(), name='contact_view')
]