from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView

from contact.forms import ContactForm
from contact.models import Contact


class ContactTemplateView(CreateView):
    template_name = '../templates/contact.html'
    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('homepage')
    #
    # def form_valid(self, form):
    #         if form.is_valid():
    #             user = form.save(commit=False)
    #             user.save()
    #             send_mail(subject=f'Contacted by {Contact.fullname}',
    #                       message=f'Mail sent from: {Contact.email}, Name: {Contact.fullname}, Message: {Contact.message}',
    #                       from_email=f'{Contact.email}',
    #                       recipient_list=['cristian.belei96@gmail.com'])
    #         return redirect('homepage')
