from django.db import models


class Contact(models.Model):
    fullname = models.TextField(max_length=60, null=True, blank=False)
    email = models.EmailField(null=True, blank=True)
    subject = models.CharField(max_length=100)
    message = models.TextField(max_length=255)

    def __str__(self):
        return self.email
