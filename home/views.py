from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView, DetailView

from home.models import Project


def index(request):
    return HttpResponse


class HomeTemplateView(TemplateView):
    template_name = 'homepage.html'

# class ProjectDetailView(DetailView):
#     template_name = 'project.html'
#     model = Project
#     context_object_name = 'projects'


def project_detail(request, pk):
    project = Project.objects.get(id=pk)
    context = {'project': project}
    return render(request, 'project.html', context)


def projects_list(request, pk):
    projects = Project.objects.all()
    context = {'projects': projects}

    return render(request, 'projects.html', context)