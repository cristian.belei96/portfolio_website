from contact.forms import ContactForm
from home.models import Project


def common_data(request):
    context = {
        'contact_form': ContactForm(),
        # 'pottfolio': Project.objects.all(),
    }
    return context


def all_projects(request):
    return {'projects': Project.objects.all()}
