from django.urls import path
from home import views

urlpatterns = [
    # path('index/', views.index, name='index'),
    path('', views.HomeTemplateView.as_view(), name='homepage'),
    path('project-detail/<int:pk>/', views.project_detail, name='project_detail'),
    path('all-projects', views.projects_list, name='all_projects')

]
