from django.db import models


class Project(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    description = models.TextField(max_length=250, null=True, blank=True)
    image = models.ImageField(upload_to='static/images/projects/')
    date = models.DateTimeField(auto_now_add=True)
    project_link = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name