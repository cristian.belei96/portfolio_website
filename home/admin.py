from django.contrib import admin

from home.models import Project

admin.site.register(Project)