# Generated by Django 4.0.3 on 2022-05-15 14:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('description', models.TextField(blank=True, max_length=250, null=True)),
                ('image', models.ImageField(upload_to='.../static/images/')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('project_link', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.DeleteModel(
            name='Portfolio',
        ),
    ]
